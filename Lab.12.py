import json


def load_json(filename):
    try:
        with open(filename, 'r') as file:
            data = json.load(file)
        return data
    except (FileNotFoundError, json.JSONDecodeError):
        return []


def save_json(filename, data):
    try:
        with open(filename, 'w') as file:
            json.dump(data, file, indent=2)
        print(f"Дані успішно збережено у файлі {filename}.")
    except Exception as e:
        print(f"Помилка при збереженні даних у файлі {filename}: {e}")


def print_json(data):
    print(json.dumps(data, indent=2))


def add_entry(data):
    name = input("Введіть назву держави: ")
    population = float(input("Введіть чисельність населення (в мільйонах жителів): "))
    area = float(input("Введіть площу (в тисячах квадратних км): "))

    entry = {'name': name, 'population': population, 'area': area}
    data.append(entry)
    print("Дані успішно додано.")


def remove_entry(data, entry_name):
    for entry in data:
        if entry['name'] == entry_name:
            data.remove(entry)
            print(f"Дані про {entry_name} успішно видалено.")
            return
    print(f"Помилка: Дані про {entry_name} не знайдено.")


def search_by_field(data, field):
    search_value = input(f"Введіть значення для пошуку за полем {field}: ")
    found_entries = [entry for entry in data if str(entry.get(field, '')) == search_value]

    if found_entries:
        print("Знайдені записи:")
        print_json(found_entries)
    else:
        print(f"Записів зі значенням {search_value} за полем {field} не знайдено.")


def solve_task(data):
    max_density_country = max(data, key=lambda x: x['population'] / x['area'])
    print("Держава з максимальною щільністю населення:")
    print_json(max_density_country)

    # Запис результатів у новий JSON файл
    save_json("result.json", [max_density_country])


def get_number_of_countries():
    while True:
        try:
            num_countries = int(input("Введіть кількість держав (не більше 10): "))
            if 1 <= num_countries <= 10:
                return num_countries
            else:
                print("Неправильне значення. Будь ласка, введіть число від 1 до 10.")
        except ValueError:
            print("Неправильний формат. Будь ласка, введіть ціле число.")


def main():
    filename = "countries.json"
    data = load_json(filename)

    while True:
        print("\nМеню:")
        print("1. Виведення на екран вмісту JSON файлу")
        print("2. Додавання нового запису у JSON файл")
        print("3. Видалення запису з JSON файлу")
        print("4. Пошук даних у JSON файлі за одним із полів")
        print("5. Розв’язання завдання та запис результатів у новий JSON файл")
        print("6. Вихід")

        choice = input("Оберіть опцію (1-6): ")

        if choice == '1':
            print_json(data)
        elif choice == '2':
            num_countries = get_number_of_countries()
            for _ in range(num_countries):
                add_entry(data)
            save_json(filename, data)
        elif choice == '3':
            entry_name = input("Введіть назву держави для видалення: ")
            remove_entry(data, entry_name)
            save_json(filename, data)
        elif choice == '4':
            field = input("Введіть поле для пошуку: ")
            search_by_field(data, field)
        elif choice == '5':
            solve_task(data)
        elif choice == '6':
            print("До побачення!")
            break
        else:
            print("Неправильний вибір. Оберіть опцію знову.")


if __name__ == "__main__":
    main()
